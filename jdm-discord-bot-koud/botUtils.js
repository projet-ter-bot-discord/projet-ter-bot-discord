import axios from "axios";
import { JSDOM } from "jsdom";
import { extractTermsAndRelation } from "./parser.js";
import { formatReply } from "./formatBotReply.js";

const formatUrl = (term1, term2, rel) => {
    return `https://www.jeuxdemots.org/rezo-ask.php?gotermsubmit=Ask&term1=${term1}&rel=${rel}&term2=${term2}`
}


async function getHtmlFromEndpoint(url) {
    try {
        const response = await axios.get(url);
        return response.data;
    } catch (error) {
        console.error('Error fetching HTML:', error);
        throw error;
    }
}

function getTextFromElement(html, selector) {
    const dom = new JSDOM(html);
    const selectedElement = dom.window.document.querySelector(selector);
    if (selectedElement) {
        return selectedElement.textContent.trim();
    } else {
        throw new Error(`Element with selector "${selector}" not found`);
    }

}

export async function findRelScore(term1, relType, term2) {
    try {
        let url = formatUrl(term1, term2, relType);
        const html = await getHtmlFromEndpoint(url);
        const score = getTextFromElement(html, "result > w");
        return score;
    }
    catch (error) {
        console.error(error);
    }
}


export async function botAnswer(question) {
    try {
        let termsAndRelation = extractTermsAndRelation(question);

        const term1 = termsAndRelation.term1;
        const term2 = termsAndRelation.term2;
        const relType = termsAndRelation.relType;

        const score = await findRelScore(term1, relType, term2);
        const reply = formatReply(termsAndRelation, score)

        return {
            reply: reply,
            termsAndRelation: termsAndRelation
        };
    } catch (error) {
        throw new Error(error);
    }
}
