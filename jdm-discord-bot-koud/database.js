import { MongoClient } from "mongodb";


/**
 * 
 * @param {string} url the connection url
 * @param {string} dbName the name of the created database
 * @returns  {object[client,db]}  an object containing the client and the database object
 */
export async function connectToDatabase(url, dbName) {
    const client = new MongoClient(url);
    try {
        await client.connect();
        console.log('Connected successfully to database');
        const db = client.db(dbName);
        return { client, db };
    } catch (err) {
        console.error('Failed to connect to the database', err);
        throw err;
    }
}


/**
 * 
 * @param {object} db A database object created by connectToDatabase function
 * @param {string} collectionName name of collection
 * @param {object} document the object to be added as a document
 */
export async function createDocument(db, collectionName, document) {
    try {
        const collection = db.collection(collectionName);
        const result = await collection.insertOne(document);
    } catch (err) {
        throw err;
    }
}

/**
 * 
 * @param {string} db Database name
 * @param {string} collectionName Collection name of the MongoDB database
 * @param {Object} query A query object to query the database
 * @returns {Object} the queried document
 */
export async function queryDocuments(db, collectionName, query) {
    try {
        const collection = db.collection(collectionName);
        const documents = await collection.find(query).toArray();
        return documents[0];
    } catch (err) {
        throw err;
    }
}