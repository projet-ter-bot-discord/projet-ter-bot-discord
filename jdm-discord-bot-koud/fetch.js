import { JSDOM } from "jsdom";
import { existsSync, mkdirSync, readFileSync, writeFileSync } from "node:fs";
import { join } from "node:path";
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import logger from './config/logger.js'; // Ajustez le chemin selon l'endroit où vous placez le fichier
import fetch from 'node-fetch'; // Assurez-vous d'importer fetch si vous utilisez node-fetch version 3 ou supérieure

// Exemple d'utilisation
logger.info('This is an informational message');
logger.error('This is an error message');

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const dataDirectory = join(__dirname, "data");

function sanitizeFilename(name) {
  return name.replace(/[^a-z0-9]/gi, '_').toLowerCase();
}

export const fetchAndExtractData = async (keyword, relationNo) => {
  const sanitizedKeyword = sanitizeFilename(keyword);
  const filePath = join(dataDirectory, `JDM_${sanitizedKeyword}_${relationNo}.html`);

  if (!existsSync(dataDirectory)) {
    mkdirSync(dataDirectory);
  }

  try {
    if (existsSync(filePath)) {
      logger.info(`Reading data from the filesystem for keyword: ${sanitizedKeyword} and relationNo: ${relationNo}`);
      const data = readFileSync(filePath, "utf-8");
      const dom = new JSDOM(data);
      return extractDataFromDocument(dom.window.document, filePath);
    } else {
      const url = `https://www.jeuxdemots.org/rezo-dump.php?gotermsubmit=Chercher&gotermrel=${encodeURIComponent(sanitizedKeyword)}&rel=${relationNo}`;
      console.log("Fetching data from the web...", url);
      const response = await fetch(url);
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const text = await response.text();
      console.log("Received data:", text); // Ajoutez cette ligne pour voir le contenu récupéré
      const dom = new JSDOM(text);
      writeFileSync(filePath, text);
      return extractDataFromDocument(dom.window.document, url);
    }
  } catch (error) {
    console.error(`Failed to fetch and extract data: ${error}`);
    return null;
  }
};

function extractDataFromDocument(document, url) {
  try {
    const codeElement = document.querySelector("CODE");
    if (!codeElement) {
      console.error("No CODE tag found in the document for URL:", url);
      return { error: "No CODE tag found", url };
    }

    return parseDocument(codeElement.textContent, url);
  } catch (error) {
    console.error("Error parsing document:", error);
    return { error: "Error parsing document", url };
  }
}

function parseDocument(codeContent, url) {
  if (!codeContent) {
    console.error("No CODE tag found in the document for URL:", url);
    return null;
  }

  return {
    entries: parseSection(codeContent, /\/\/ les noeuds\/termes.*?\n(.*?)\/\/ les types de relations/s),
    outgoingRelations: parseSection(codeContent, /\/\/ les relations sortantes.*?\n(.*?)\/\/ les relations entrantes/s)
  };
}

function parseSection(text, regex) {
  const match = text.match(regex);
  return match && match[1] ? match[1].trim().split("\n").map(parseLine) : [];
}

function parseLine(line) {
  return line.split(";").map(part => part.replaceAll("'", "").trim());
  console.log(reponse);
}
