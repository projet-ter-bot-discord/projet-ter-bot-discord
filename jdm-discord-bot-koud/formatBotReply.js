const mapRelFormatreplyfunc = {
    "r_agent-1": formatReplyRAgent_1,
    "r_isa": formatReplyRIsa,
    "r_lieu": formatReplyRLieu,
}

/**
 * function to get a reply that the bot can use using termsAndRelation and score
 * @param {object} termsAndRelation 
 * @param {number} score 
 * @returns the a reply properly formatted for the bot to reply
 */
export function formatReply(termsAndRelation, score) {
    const relType = termsAndRelation.relType;
    let functionForReply = mapRelFormatreplyfunc[relType];

    if (score == 0) {
        return "Je ne sais pas. Pouvez-vous me dire?";
    }
    else {
        return functionForReply(termsAndRelation, score);
    }
}

function formatReplyRAgent_1(termsAndRelation, score) {
    const agent = termsAndRelation.term1;
    const verbe = termsAndRelation.term2;
    const article = termsAndRelation.article;
    const auxVerbe = termsAndRelation.auxVerbe;

    const reply = score > 0
        ? `Oui, ${article} ${agent} ${auxVerbe} ${verbe}.`
        : `Non, ${article} ${agent} ne ${auxVerbe} pas ${verbe}.`

    return reply;
}

function formatReplyRIsa(termsAndRelation, score) {
    const term1 = termsAndRelation.term1;
    const term2 = termsAndRelation.term2;
    const article = termsAndRelation.article;
    const verbe = termsAndRelation.verbe;

    const reply = score > 0
        ? `Oui, ${term1} ${verbe} ${article} ${term2}.`
        : `Non, ${term1} n'${verbe} pas ${article} ${term2}.`

    return reply;
}

function formatReplyRLieu(termsAndRelation, score) {
    const term1 = termsAndRelation.term1;
    const term2 = termsAndRelation.term2;
    const preposition = termsAndRelation.preposition;
    const verbe = termsAndRelation.verbe;

    const reply = score > 0
        ? `Oui, ${term1} ${verbe} ${preposition} ${term2}.`
        : `Non, ${term1} n'${verbe} pas ${preposition} ${term2}.`

    return reply;
}