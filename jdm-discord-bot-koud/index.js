import { fetchAndExtractData } from "./fetch.js";
import readline from "node:readline/promises";
import process from "node:process";

const findKeyword = (keyword, relatedKeywords) => {
  return relatedKeywords.find((r) =>
    r[2]?.toLowerCase().includes(keyword.toLowerCase())
  );
};

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

while (true) {
  const keyword1 = await rl.question("Enter the first keyword: ");
  const relation = await rl.question("Enter the relation number: ");
  const keyword2 = await rl.question("Enter the second keyword: ");

  const transitiveRelations = [6,5,8,9];
  const res = await fetchAndExtractData(keyword1, relation);
  if (!res) continue
  const found = findKeyword(keyword2, res.entries);

  if (found) {
    console.log(
      `The keyword "${keyword2}" was found
       in the related keywords of "${keyword1}".`
    );

    // Scan the outgoing relations for a relation with positive weight
    const foundOutgoingRelation = res.outgoingRelations.find(
      (r) => r[3] === found?.[1] && r[5] > 0
    );

    if (foundOutgoingRelation) {
      console.log(
        `The relation "${foundOutgoingRelation[4]}" has a positive weight of ${foundOutgoingRelation[5]}`
      );
    } else {
      console.log("No relation with positive weight found");
    }
  } else if (!found ) {
    // If the keyword2 is not found in the direct entries of keyword1 (TRANSITIVITE)
    for(let i = 0; i < transitiveRelations.length; i++) {
    
    const res1 = await fetchAndExtractData(keyword1, transitiveRelations[i]);
    let found2;
    let entry;
    let res2;

    for (let i = 0; i < res1.entries.length; i++) {

      res2 = await fetchAndExtractData(res1.entries[i][2], relation);
      if (!res2) continue

      found2 = findKeyword(keyword2, res2.entries);

      if (found2) {
        entry = res1.entries[i][2];
        console.log(
          `The keyword "${keyword2}" was ${
            found2 ? "found" : "NOT found"
          } in the related keywords of "${entry}".\n`
        );

        break;
      }
    }
  

    // Scan the outgoing relations for a relation with positive weight
    const foundOutgoingRelation = res2.outgoingRelations.find(
      (r) => r[3] === found2?.[1] && r[5] > 0
    );

    if (foundOutgoingRelation) {
      console.log("Found by transitivity.\n");
      console.log(`"${keyword1}", "${transitiveRelations[i]}", "${entry}"`);
      console.log(`"${entry}", "${foundOutgoingRelation[4]}", "${keyword2}"`);
      console.log(
        `So "${keyword1}", "${foundOutgoingRelation[4]}", "${keyword2}"\n`
      );
      console.log(
        `The relation "${foundOutgoingRelation[4]}" has a positive weight of ${foundOutgoingRelation[5]}\n`
      );
      break;
    } else {
      console.log("No relation with positive weight found");
    }

  }
  }

  console.log(
    `The keyword "${keyword2}" was ${
      found ? "found" : "NOT found"
    } in the direct related entries of "${keyword1}".`
  );
}