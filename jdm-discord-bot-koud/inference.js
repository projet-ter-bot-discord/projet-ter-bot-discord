import { fetchAndExtractData } from "./fetch.js";
import { JDM } from "./models.js";
import knowledgeBase from './knowledge_base.json' assert { type: "json" };

function inferTransitivity(termA, relation, termB) {
  const intermediateTerms = knowledgeBase.filter(k => k.term === termA && k.relations.some(r => r.type === relation));
  return intermediateTerms.some(intermediate => {
    const targetTerm = intermediate.relations.find(r => r.type === relation)?.target;
    if (!targetTerm) return false;
    return knowledgeBase.some(k => k.term === targetTerm && k.relations.some(r => r.type === relation && r.target === termB));
  });
}

function inferInductionDeduction(termA, relation) {
  const relatedTerms = knowledgeBase.filter(k => k.relations.some(r => r.target === termA && r.type === relation));
  return relatedTerms.map(rt => {
    const relationInfo = rt.relations.find(r => r.target === termA && r.type === relation);
    return {
      type: relationInfo?.inferred ? 'Deduction' : 'Induction',
      term: rt.term,
      relation: relation,
      target: termA
    };
  }).filter(rt => rt.type); // Only return non-null results
}

function inferSymmetry(termA, relation, termB) {
  return knowledgeBase.some(k => k.term === termB && k.relations.some(r => r.target === termA && r.type === relation));
}

export async function verifyDirectRelation(termA, relation, termB) {
  if (!termA || !termB || !relation) {
    console.error('One or more required parameters are undefined.', { termA, relation, termB });
    return { linked: false, message: "Les termes et la relation doivent être définis." };
  }

  const knowledgeEntry = knowledgeBase.find(k =>
    k.term.toLowerCase() === termA.toLowerCase() &&
    k.relations.some(r => r.target.toLowerCase() === termB.toLowerCase() && r.type === relation)
  );
  
  if (knowledgeEntry) {
    return { linked: true, message: `${termB} est directement lié à ${termA} car ${knowledgeEntry.relations.find(r => r.target.toLowerCase() === termB.toLowerCase()).explanation}` };
  }

  if (inferTransitivity(termA, relation, termB)) {
    return { linked: true, message: `Transitive relation found between ${termA} and ${termB}` };
  }

  const response = await fetchAndExtractData(termA, relation);
  if (!response || !response.entries) {
    console.error('Failed to fetch data for:', termA, relation);
    return { linked: false, message: `Impossible de vérifier la relation entre ${termA} et ${termB}.` };
  }

  const jdmInstance = new JDM(termA, relation, null, response);
  const entry = jdmInstance.searchEntries(termB);
  const isLinked = entry && response.entries.some(e => e.term && e.term.toLowerCase() === termB.toLowerCase());
  return { linked: isLinked, message: isLinked ? `${termB} est directement lié à ${termA}` : `${termB} n'est pas directement lié à ${termA}` };
}

export async function deduceRelation(termA, relation, termB) {
  const response = await fetchAndExtractData(termA, relation);
  if (!response) {
    console.error('Failed to fetch data for:', termA, relation);
    return false;
  }

  if (inferSymmetry(termA, relation, termB)) {
    return { linked: true, message: `Symmetric relation exists between ${termA} and ${termB}` };
  }

  const deductions = inferInductionDeduction(termA, relation);
  if (deductions.length > 0) {
    return { success: true, deductions };
  }

  const hasRelation = jdmInstance.searchOutgoingRelations(termB).some(rel => rel && rel.weight > 0);
  return hasRelation;
}
