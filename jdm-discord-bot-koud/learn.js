import { createDocument } from "./database.js";

/**
 * 
 * @param {string} userReply the reply from the user
 * @param {object} termsAndRelationObj the terms and relation object
 * @param {object} db database object returned from connectToDatabase
 * @param {string} collectionName name of the collection
 * @returns 
 */
export async function learnFromUser(
    userReply,
    termsAndRelationObj,
    db,
    collectionName) {
    const formattedUserReply = userReply.toLowerCase().replace(/\s/g, '');
    if (formattedUserReply.startsWith("oui")) {
        termsAndRelationObj.score = 30;
    }
    else if (formattedUserReply.startsWith("non")) {
        termsAndRelationObj.score = -1;
    }

    createDocument(
        db,
        collectionName,
        termsAndRelationObj
    );

    return !(!termsAndRelationObj.score);
}