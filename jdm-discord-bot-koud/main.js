// discord.js
import { Client, GatewayIntentBits } from 'discord.js';
import { config } from 'dotenv';
import { botAnswer } from './botUtils.js';
import { verifyDirectRelation, deduceRelation } from './inference.js';

import { MongoClient } from 'mongodb';
import { connectToDatabase, queryDocuments } from './database.js';
import { learnFromUser } from './learn.js';
import { formatReply } from './formatBotReply.js';

config();

const dbUrl = process.env.MONGODB_URL;
const dbName = "bot-knowledge-base";
const collectionName = "term-rels";

const bot = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.MessageContent,
  ],
});

let dbConnObj;
try {
  dbConnObj = await connectToDatabase(dbUrl, dbName);
}
catch (error) {
  console.error(`Your Mongo db connection is not set up. Your bot won't 
  learn anything.`);
}

bot.login(process.env.TOKEN);

bot.on('ready', () => {
  console.log(`${bot.user.tag} est en ligne!`);
});

// Handling messages for natural language processing
bot.on('messageCreate', async message => {
  if (message.author.bot) return; // Ignore messages from the bot itself

  const content = message.content;
  if (content.startsWith('!ask')) {
    try {
      let botReply = await botAnswer(content);
      if (botReply.termsAndRelation.score != 0) {
        message.channel.send(botReply.reply);
      }

      const botReplyIdk = "Je ne sais pas. Pouvez-vous me dire?";
      if (botReply.reply == botReplyIdk) {
        try {
          let existingKnowledge = await queryDocuments(
            dbConnObj.db, collectionName, botReply.termsAndRelation);

          if (existingKnowledge) {
            let existingKnowledgeScore = existingKnowledge.score;
            delete existingKnowledge._id;
            delete existingKnowledge.score;
            botReply.reply = formatReply(existingKnowledge, existingKnowledgeScore);
            message.channel.send(botReply.reply);
          }
          else {
            message.channel.send(botReply.reply);
            try {
              const filter = response => response.author.id === message.author.id;
              const collected = await message.channel.awaitMessages({
                filter,
                max: 1,
                time: 30000,
                errors: ['time']
              });

              let userReply = collected.first().content;
              learnFromUser(
                userReply,
                botReply.termsAndRelation,
                dbConnObj.db,
                collectionName
              )
              await message.channel.send("Merci pour votre réponse. Je m'en souviendrai.");
            } catch (err) {
              await message.channel.send("Vous n'avez pas répondu à temps !");
            }

          }

        }
        catch (e) {
          console.log(e);
          message.channel.send("Database is not set up. Bot cannot learn.");
        }
      }
    }
    catch (error) {
      console.log({ error });
      message.channel.send("An error occurred while processing your request.");
    }
  }


  else if (content.startsWith('!')) {
    handleMessage(message);
  }
});

// Function to handle command-based interactions
async function handleMessage(message) {
  if (message.author.bot) return;
  const args = message.content.slice(1).split(' ');
  const command = args.shift().toLowerCase();

  if (command === 'check' || command === 'deduce') {
    if (args.length < 3) {
      message.channel.send("Please make sure your command includes two terms and a relation. Example: `!check termA r_relation termB`");
      return;
    }

    const termA = args[0];
    const relation = args[1];
    const termB = args[2];

    try {
      const result = command === 'check' ? await verifyDirectRelation(termA, relation, termB) : await deduceRelation(termA, relation, termB);
      message.channel.send(result.message);
    } catch (error) {
      console.error('Error processing command:', error);
      message.channel.send('An error occurred while processing your request.');
    }
  }
}

