import { fetchAndExtractData } from "./fetch.js";
import { JSDOM } from "jsdom";

export const JDM = class {
  /**
   * Deductive and inductive relations
   * 6: is_a, Il est demandé d'énumérer les GENERIQUES/hyperonymes du terme. Par exemple, 'animal' et 'mammifère' sont des génériques de 'chat'. - [conversif : r_hypo] - [1 ≤ q=1 ≤ 2]
   * 8: r_hypo, Il est demandé d'énumérer des SPECIFIQUES/hyponymes du terme. Par exemple, 'mouche', 'abeille', 'guêpe' pour 'insecte'. - [conversif : r_isa] - [1 ≤ q=1 ≤ 2]
   * 9: r_has_part, Il faut donner des PARTIES/constituants/éléments (a pour méronymes) du mot cible. Par exemple, 'voiture' a comme parties : 'porte', 'roue', 'moteur', ... - [conversif : r_holo] - [1 ≤ q=1 ≤ 2]
   * 5: r_syn, Il est demandé d'énumérer les synonymes ou quasi-synonymes de ce terme. - [conversif : r_syn] - [1 ≤ q=1.88 ≤ 5]
   */
  static deductiveInductiveRelations = [6, 8, 9, 5];

  /**
   * Transitive relations
   * 15: r_lieu, Il est demandé d'énumérer les LIEUX typiques où peut se trouver le terme/objet en question. (carotte r_lieu potager) - [conversif : r_lieu-1] - [1 ≤ q=1 ≤ 2]
   */
  static transitiveRelations = [15];

  static parseLine(line) {
    const parts = line.split(";");

    return parts.map((part) => {
      part = part.trim().replaceAll("'", "").toLowerCase();

      // TODO: handle the case where the label contains a '>'
      return part;
    });
  }

  constructor(keyword, relationNo, html, json) {
    if (json) {
      this.documentContent = null;
      this.keyword = json.keyword;
      this.relationNo = json.relationNo;
      this.entries = json.entries;
      this.outgoingRelations = json.outgoingRelations;
      return;
    }

    const dom = new JSDOM(html);

    this.documentContent =
      dom.window.document.querySelector("code")?.textContent;

    if (!this.documentContent) {
      throw new Error("No content found in the document");
    }

    this.keyword = keyword;
    this.relationNo = relationNo;
    this.entries = this.extractEntries();
    this.outgoingRelations = this.extractOutgoingRelations();
  }

  // les noeuds/termes (Entries) : e;eid;'name';type;w;'formated name'
  extractEntries() {
    const entriesRegExp =
      /\/\/ les noeuds\/termes.*?\n(?<entries>.*?)\/\/ les types de relations/su;
    const entriesSection =
      this.documentContent.match(entriesRegExp)?.groups.entries;

    if (!entriesSection) {
      throw new Error("No entries section found in the document");
    }

    const entries = entriesSection
      .split("\n")
      .filter((line) => {
        return line.startsWith("e;");
      })
      .map(JDM.parseLine);

    return entries;
  }

  searchEntries(keyword) {
    if (typeof keyword === "number") {
      keyword = String(keyword);
      return this.entries.find((entry) => {
        return entry[1] === keyword;
      });
    }

    if (typeof keyword === "string") {
      return this.entries.find((entry) => {
        return entry[2] === keyword;
      });
    }
  }

  // les relations sortantes (Outgoing Relations) : r;rid;node1;node2;type;w;w_normed;rank
  extractOutgoingRelations() {
    const relationsRegExp =
      /\/\/ les relations sortantes.*?\n(?<relations>.*?)\/\/ les relations entrantes/su;
    const relationsSection =
      this.documentContent.match(relationsRegExp)?.groups.relations;

    if (!relationsSection) {
      throw new Error("No relations section found in the document");
    }

    const relations = relationsSection
      .split("\n")
      .filter((line) => {
        return line.startsWith("r;");
      })
      .map(JDM.parseLine);

    return relations;
  }

  searchOutgoingRelations(keyword) {
    if (typeof keyword === "number") {
      keyword = String(keyword);
      return this.outgoingRelations.find((relation) => {
        return relation[1] === keyword;
      });
    }

    if (typeof keyword === "string") {
      return this.outgoingRelations.find((relation) => {
        return keyword === relation[3] && relation[6] > 0;
      });
    }
  }

  async searchPositiveRelation(keyword, transitive = true) {
    const entry = this.searchEntries(keyword);

    if (!entry) {
      if (transitive) {
        // Search for transitive relations
        return await this.searchTransitivePositiveRelation(keyword);
      }

      return null;
    }

    const outgoingRelation = this.searchOutgoingRelations(entry[1]);
    if (!outgoingRelation) {
      return null;
    }

    return [entry, outgoingRelation];
  }

  async searchTransitivePositiveRelation(keyword) {
    for (const relationNo of JDM.deductiveInductiveRelations) {
      const transitiveJDM = await fetchAndExtractData(this.keyword, relationNo);
      if (!transitiveJDM) {
        continue;
      }

      // For instance transitiveJDM can be "eagle is_a", so for each entry of "is_a" we check if the keyword is found
      // We order the outgoing relations by weight, so we can check the most relevant ones first
      transitiveJDM.outgoingRelations.sort((a, b) => {
        return Number(b[6]) - Number(a[6]);
      });

      for (
        let i = 0;
        i < Math.min(transitiveJDM.outgoingRelations.length, 5);
        ++i
      ) {
        // We convert to number to check for the Id in searchEntries()
        const entityId = Number(transitiveJDM.outgoingRelations[i][3]);
        if (Number.isNaN(entityId)) {
          continue;
        }

        const entry = transitiveJDM.searchEntries(entityId);
        const jdm = await fetchAndExtractData(entry[2], this.relationNo);
        if (!jdm) {
          continue;
        }

        const res = await jdm.searchPositiveRelation(keyword, false);
        if (!res) {
          continue;
        }

        console.warn(`Found: "${this.keyword}" "${relationNo}" "${entry[2]}"`);

        return res;
      }
    }
  }

  toJSON() {
    return {
      documentContent: null,
      keyword: this.keyword,
      relationNo: this.relationNo,
      entries: this.entries,
      outgoingRelations: this.outgoingRelations,
    };
  }
};