const mapQuerytypeFunc =
{
    "estce_que": extractObjsEstceque,
    "isa": extractObjsIsa,
    "location_one_verb": extractObjsLocationOneVerb,
}


function matchRegex(question) {
    const formattedQuestion = question.replace("!ask ", "");

    const regexPatterns = [
        {
            pattern: /est-ce\s+(?:que|qu')\s*(un|les|une)\s+(\p{L}+)\s+(?:ne\s+)?(\p{L}+)\s+(?:pas\s+)?(\p{L}+)\s?\?/iu,
            queryType: 'estce_que',
            relType: "r_agent-1"
        },
        {
            pattern: /(un|une|les|des)\s+(\p{L}+)\s+(?:ne\s+)?(\p{L}+)(?:-\w+)\s+(?:pas\s+)?(\p{L}+)\s*\?/iu,
            queryType: 'estce_que',
            relType: "r_agent-1"
        },
        {
            pattern: /(?:la|le|les|des|un|une)\s+?(\p{L}+)\s+(?:n|)?e?\s*'?\s*(est|sont)-?(?:il|elles)?\s+(?:pas\s+)?(un|une|la|le|les|des)\s+(\p{L}+)\s*\?/iu,
            queryType: 'isa',
            relType: "r_isa"
        },
        {
            pattern: /(?:la\s+)?(\p{L}+(\s+\p{L}+)*)\s+(?:c')?(est)(?:-(?:il|elle))?\s+(en|à)\s+(\p{L}+)\s*\?/iu,
            queryType: 'location_one_verb',
            relType: "r_lieu"
        }
    ];

    for (let regex of regexPatterns) {
        let match = formattedQuestion.match(regex.pattern);
        if (match && match[0] === formattedQuestion) {
            return {
                match: match,
                regexObj: regex
            };
        }
    }
    return null;
}

/**
 * 
 * @param {string} question 
 * @returns {Object} object containing the terms, relType, and others
 * all formatted in the proper way according to the proper queryType 
 * functions
 */
export function extractTermsAndRelation(question) {
    const matchedRegexObj = matchRegex(question);
    const matchArray = matchedRegexObj.match;
    const queryType = matchedRegexObj.regexObj.queryType;
    const funcToUse = mapQuerytypeFunc[queryType];

    const formattedObj = funcToUse(matchArray);
    return formattedObj;
}

/**
 * extracts objects from match array
 * @function
 * @param {Array} matchArray an array of match objects obtained from 
 * matchedRegexObject
 * @returns {Object} an object with array values assigned to proper keys for
 * question type "est-ce que"
 */
function extractObjsEstceque(matchArray) {
    return {
        article: matchArray[1],
        term1: matchArray[2],
        auxVerbe: matchArray[3],
        term2: matchArray[4],
        relType: "r_agent-1"
    }
}


function extractObjsIsa(matchArray) {
    return {
        term1: matchArray[1],
        verbe: matchArray[2],
        article: matchArray[3],
        term2: matchArray[4],
        relType: "r_isa"
    }
}

function extractObjsLocationOneVerb(matchArray) {
    return {
        term1: matchArray[1],
        verbe: matchArray[3],
        preposition: matchArray[4],
        term2: matchArray[5],
        relType: "r_lieu"
    }
}