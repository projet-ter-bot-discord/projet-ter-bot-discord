import { botAnswer } from "../botUtils.js";
import { learnFromUser } from "../learn.js";
import { connectToDatabase, queryDocuments } from "../database.js";
import { config } from "dotenv";
import { MongoClient } from "mongodb";


test(`Bot can answer questions about capability when the score is positive
or negative`,
  async () => {
    const replyPositive = await botAnswer("un oiseau peut-il voler?");
    // const replyNegative = 

    expect(replyPositive.reply)
      .toBe("Oui, un oiseau peut voler.");
  }
)


test(`Bot asks about questions where the score is 0 and replies with
"I don't know"`,
  async () => {
    const replyObj = await botAnswer("un tableau peut-il débattre?");

    expect(replyObj.reply)
      .toBe("Je ne sais pas. Pouvez-vous me dire?");
  }
)

test(`Bot learns from user reply`,
  async () => {
    config()
    const dbUrl = process.env.TEST_DB_URL;
    const dbName = "test-learning";
    const collectionName = "learned-test-coll";
    const dbConnObj = await connectToDatabase(dbUrl, dbName);

    const replyObj = await botAnswer("un tableau peut-il débattre?");
    const userReply = "Non, un tableau ne pas peut débattre.";

    await learnFromUser(
      userReply,
      replyObj.termsAndRelation,
      dbConnObj.db,
      collectionName
    );

    const learnedRelation = await queryDocuments(
      dbConnObj.db,
      collectionName,
      replyObj.termsAndRelation
    );

    const expectedLearnedRelation = {
      term1: "tableau",
      term2: "débattre",
      relType: "r_agent-1",
      score: -1
    };
    expect(learnedRelation).toContainEqual(expectedLearnedRelation);
  }
)

afterAll(async () => {
  config();
  const dbUrl = process.env.TEST_DB_URL;
  const client = new MongoClient(dbUrl);
  const db = client.db("test-learning");
  const testCollection = db.collection("learned-test-coll");
  testCollection.deleteOne({
    term1: "tableau",
    term2: "débattre",
    relType: "r_agent-1",
    score: -1
  });
  // client.close();
})