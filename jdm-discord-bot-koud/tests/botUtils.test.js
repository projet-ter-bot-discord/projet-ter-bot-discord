import { botAnswer } from "../botUtils.js";

test("returns bot answer after processing can question",
    async () => {
        const questionSingularEstceQue = "!ask Est-ce que un oiseau peut voler?";
        const questionPluralEstceQue = "!ask Est-ce que les crocodiles peuvent nager?";
        const questionEstceQuUn = "!ask Est-ce qu'un canard peut voler?";

        const singularBotAnswer = await botAnswer(questionSingularEstceQue);
        const pluralBotAnswer = await botAnswer(questionPluralEstceQue);
        const estceQuUn = await botAnswer(questionEstceQuUn);
        expect(singularBotAnswer.reply)
            .toBe("Oui, un oiseau peut voler.");

        expect(pluralBotAnswer.reply)
            .toBe("Oui, les crocodiles peuvent nager.");

        expect(estceQuUn.reply)
            .toBe("Oui, un canard peut voler.");
    }
)

test("returns bot answer after processing isa question",
    async () => {
        const questionSingularIsa = "!ask Un chat n ' est-il pas un animal?";

        const singularBotAnswer = await botAnswer(questionSingularIsa);
        expect(singularBotAnswer.reply)
            .toBe("Oui, chat est un animal.");

    }
)

test(`returns bot answer to capability questions that starts with nouns
e.g. "Les enfants peuvent-ils jouer?"`,
    async () => {
        const questionSingularCanNounfirst = "!ask un poisson peut-il nager ?";
        const questionPluralCanNounFirst = "!ask les crocodiles peuvent-ils voler ?";

        const singularBotAnswer = await botAnswer(questionSingularCanNounfirst);
        const pluralBotAnswer = await botAnswer(questionPluralCanNounFirst);
        expect(singularBotAnswer.reply)
            .toBe("Oui, un poisson peut nager.")
        expect(pluralBotAnswer.reply)
            .toBe("Non, les crocodiles ne peuvent pas voler.")
    }
)


test(`returns bot answer to questions about location where the format is simply
"Is this_place in that_place?"`,
    async () => {
        const questionOneWordPlace = "!ask Paris c'est en France ?";
        const questionMultiWordPlace = "!ask La tour Eiffel est-elle à Paris ?";
        const questionNegative = "!ask Berlin est-il en France?"

        const oneWordPlaceAnswer = await botAnswer(questionOneWordPlace);
        const multiWordPlaceAnswer = await botAnswer(questionMultiWordPlace);
        const negativeAnswer = await botAnswer(questionNegative);
        expect(oneWordPlaceAnswer.reply)
            .toBe("Oui, Paris est en France.");
        expect(multiWordPlaceAnswer.reply)
            .toBe("Oui, tour Eiffel est à Paris.");
        expect(negativeAnswer.reply)
            .toBe("Non, Berlin n'est pas en France.");
    }
)