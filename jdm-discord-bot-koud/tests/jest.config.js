// jest.config.js
module.exports = {
    transform: {
      '^.+\\.js$': 'babel-jest'
    },
    testEnvironment: 'node',
    transformIgnorePatterns: ['/node_modules/'],
    moduleNameMapper: {
      '^.+\\.(css|less|scss)$': 'identity-obj-proxy'
    }
  };
  