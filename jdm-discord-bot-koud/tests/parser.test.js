import { extractTermsAndRelation } from "../parser.js";




test(`performs regex matching to find "can" patterns with the word "peut" 
in the question and returns a match array, and queryType.`,
  () => {
    const canPeutQuestions = [
      "Est-ce que un chat peut voler?",
      "Est-ce que un chat ne peut pas voler ?",
    ];
    canPeutQuestions.forEach((question, i) => {
      const termsAndRelation = extractTermsAndRelation(question);
      expect(termsAndRelation)
        .toStrictEqual(
          {
            article: "un",
            term1: "chat",
            auxVerbe: "peut",
            term2: "voler",
            relType: "r_agent-1"
          });
    })
  }
)

test(`performs regex matching to find "can" patterns with the word "pourrait" 
in the question and returns a match array, and queryType.`,
  () => {
    const canPourraitQuestions = [
      "Est-ce que un chat pourrait voler?",
      "Est-ce que un chat ne pourrait pas voler ?",
    ];
    canPourraitQuestions.forEach((question, i) => {
      const termsAndRelation = extractTermsAndRelation(question);
      expect(termsAndRelation)
        .toStrictEqual(
          {
            article: "un",
            term1: "chat",
            auxVerbe: "pourrait",
            term2: "voler",
            relType: "r_agent-1"
          });
    })
  }
)

test(`performs regex matching to find "can" patterns with plural words 
in the question and returns a match array, and queryType.`,
  () => {
    const canPluralQuestions = [
      "Est-ce que les oiseaux peuvent voler?",
      "Est-ce que les oiseaux ne peuvent pas voler ?",
    ];
    canPluralQuestions.forEach((question, i) => {
      const termsAndRelation = extractTermsAndRelation(question);
      expect(termsAndRelation)
        .toStrictEqual(
          {
            article: "les",
            term1: "oiseaux",
            auxVerbe: "peuvent",
            term2: "voler",
            relType: "r_agent-1"
          });
    })
  }
)


// test case for isa questions
test(`takes any isa object (e.g. is Falcon a bird?) and returns an object
containing the article, agent, and verb`,
  () => {
    const isaQuestions = [
      "Le faucon est-il un oiseau ?",
    ]
    isaQuestions.forEach((question, i) => {
      const termsAndRelation = extractTermsAndRelation(question);
      expect(termsAndRelation)
        .toStrictEqual(
          {

            term1: "faucon",
            verbe: "est",
            article: "un",
            term2: "oiseau",
            relType: "r_isa"

          }
        )
    })
  }
)


test(`takes any location question with a single verb, and returns 
the extract object`,
  () => {
    const isaQuestions = [
      "La tour Eiffel est-elle à Paris ?",
    ]
    isaQuestions.forEach((question, i) => {
      const termsAndRelation = extractTermsAndRelation(question);
      expect(termsAndRelation)
        .toStrictEqual(
          {
            term1: "tour Eiffel",
            verbe: "est",
            preposition: "à",
            term2: "Paris",
            relType: "r_lieu"
          }
        )
    })
  }
)